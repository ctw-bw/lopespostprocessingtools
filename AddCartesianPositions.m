function outData = AddCartesianPositions(inData, patientDimensions)
% AddCartesianPositions - Adds fields for cartesian positions to data structure
%
% outData = AddCartesianPositions(inData, [patientDimensions]) 
%
% given inData, a struct obtained from readSignalsFile, the output is the
% same as the input data, augmented with cartesian positions of pelvis,
% hips, knees and ankles. No heels and toes because we don't have any foot
% orientation sensors.
%
% patientDimensions should be a vector containing the dimensions of the
% patient as follows (all in SI):
%    [Mass, length, widthPelvis,heightPelvis,lengthThighL,lengthThighR, ...
%     lengthShankL, lengthShankR, lengthFootL,lengthFootR, heightFootL, heightFootR]
% or, 
%    if omitted, the 'default' patient is taken (obtained from the 
%    persistence file around 150101)
%
% We use the measured joint angles for our calculations.


% Check whether the compiled function is available (may not be in 32-bits versions):
if exist('KinHuman_function','file')~=3 % If it is not a mex function
    error('KinHuman_function.mexwXX does not exist. Please try to create it by doing ''mex KinHuman_function.cpp KinHuman_common.cpp KinHuman.cpp KinObject.cpp KinPointAxis.cpp'' and try again. If it doesn''t work, report to Gijs.');
end;

% Prepare the patientDimensions variable
if ~exist('patientDimensions','var')
%     patientDimensions = [80,1.80,0.2,0.2,0.45,0.45,0.45,0.45,0.25,0.25,0.1,0.1];
    thighlength=0.45;
    shanklength=0.45;
    patientDimensions = [80,1.80,0.2,0.0,thighlength,thighlength,shanklength,shanklength,0.25,0.25,0.1,0.1];
end;

% Get number of samples
NSamples = size(inData.rawdata,1);

% We now prepare a huge matrix; one row for each sample time, each row
% consists of one full input of jointAngles for the kinHuman function, as:
% jointAnglesRow= [PX, PY, PZ, PAX, PFA, LHA, LHF, LHE, LKF, LAE, LAP, LAI, RHA, RHF, RHE, RKF, RAE, RAP, RAI]
% Depending on what data is available, we can use different sets as inputs.
% Some have not all data available, or have segmentangles instead of
% jointangles (then we need to calculate jointAngles from them). Also sign
% conventions may differ. We set up a cell array of different functions, and
% try new functions until we found a suitable one (i.e., one which matches 
% the available fields in the data.
getJointAnglesFunctions = { ...
    @GetJointAnglesFromMeasMotorWithUnits, ...
    @GetJointAnglesFromMeasMotorWithoutUnits, ...
    @GetJointAnglesFromGUIMeasured, ...
    @GetJointAnglesFromGUIReference };

for i=1:length(getJointAnglesFunctions)
    fcn = getJointAnglesFunctions{i};
    jointAngles = fcn(inData.data, NSamples);
    if ~isempty(jointAngles)
        % Then the function was able to extract data, so we can continue
        break;
    end;
end;
% If we'r here and jointAngles is still empty, then there was no function
% which could extract the data.
if isempty(jointAngles)
    error('Data structure did not contain any useful data for extracting cartesian positions (or this function could not handle the data correctly).');
end;

%% Do the work. Note that assigning into the struct also takes time, so we separate the 100% between the two
RatioOfTimeForWaitBar = 0.6; % this amount of time is spent here (the rest during assignment)
cartesianPositions = zeros(NSamples, 33);
W = waitbar(0,'Please wait...');

for i=1:NSamples
    jointAnglesRow = jointAngles(i,:);
    cartesianPositions(i,:) = reshape(KinHuman_function(patientDimensions,jointAnglesRow,0),1,33);
    if mod(i,1000)==0
        waitbar(i/NSamples*RatioOfTimeForWaitBar);
    end;
end;


%% and store it into the structure. We should store it in the rawdata as well as in data.

cartesianPositions(:,[13:18,28:33])=[]; % Remove the heel and toe data (as that is based on nothing)
keyPrefix = 'Lopes.CartesianPositions.';
Points = {'Pelvis','LeftHip','LeftKnee','LeftAnkle','RightHip','RightKnee','RightAnkle'};
keys=cell(1,21);
fieldNames=cell(1,21);
for i=1:7
    base=3*(i-1)+1;
    keys{base+0} = [keyPrefix Points{i} '.x'];
    fieldNames{base+0} = strrep(keys{base+0}, '.','_');
    keys{base+1} = [keyPrefix Points{i} '.y'];
    fieldNames{base+1} = strrep(keys{base+1}, '.','_');
    keys{base+2} = [keyPrefix Points{i} '.z'];
    fieldNames{base+2} = strrep(keys{base+2}, '.','_');
end;

inData.rawdata = [ inData.rawdata cartesianPositions];
inData.map = [inData.map keys];

for i=1:21
    inData.data.(fieldNames{i}) = cartesianPositions(:,i);
    waitbar(RatioOfTimeForWaitBar + (1-RatioOfTimeForWaitBar)*i/21);
end;
close(W);   
outData = inData;   

end % of main function

%% Subfunctions
function jointAngles = GetJointAnglesFromMeasMotorWithUnits(data,NSamples) %%#ok<MSNU> #ok<DEFNU>
    % Measured position on motorencoders (get measpos) with units in the fieldname
    prefix = 'Lopes_Patient_Segments_Position_PosMeasMotor';
    if ~isfield(data,[prefix 'pelvis_tX__m_'])
        jointAngles = [];
        return
    end;
    jointAngles = zeros(NSamples, 19);
    jointAngles(:,1)  = data.([prefix 'pelvis_tX__m_']); % PX
    jointAngles(:,2)  = data.([prefix 'pelvis_tY__m_']); % PY
    jointAngles(:,3)  = data.([prefix 'pelvis_tZ__m_']); % PZ
    jointAngles(:,4)  = data.([prefix 'pelvis_ry__rad_']); % PAX
    jointAngles(:,5)  = data.([prefix 'pelvis_rx__rad_']); % PFA
    jointAngles(:,6)  = data.([prefix 'left_thigh_rX__rad_']); % LHA
    jointAngles(:,7)  = data.([prefix 'left_thigh_rZ__rad_']); % LHF
    %jointAngles(:,8)  = 0; % LHE
    jointAngles(:,9)  = data.([prefix 'left_thigh_rZ__rad_']) - data.([prefix 'left_shank_rZ__rad_']); % LKF
    %jointAngles(:,10) = 0; % LAE
    %jointAngles(:,11) = 0; % LAP
    %jointAngles(:,12) = 0; % LAI
    jointAngles(:,13) = -data.([prefix 'right_thigh_rX__rad']); % RHA
    jointAngles(:,14) = data.([prefix 'right_thigh_rZ__rad']); % RHF
    %jointAngles(:,15) = 0; % RHE 
    jointAngles(:,16) = data.([prefix 'right_thigh_rZ__rad']) - data.([prefix 'right_shank_rZ__rad']); % RKF
    %jointAngles(:,17) = 0; % RAE
    %jointAngles(:,18) = 0; % RAP
    %jointAngles(:,19) = 0; % RAI    
end
function jointAngles = GetJointAnglesFromMeasMotorWithoutUnits(data,NSamples) %#ok<DEFNU>
    % Measured position on motorencoders (get measpos) with units in the fieldname
    prefix = 'Patient_Segments_Position_PosMeasMotor';
    if ~isfield(data,[prefix 'pelvis_tX'])
        jointAngles = [];
        return
    end;
    jointAngles = zeros(NSamples, 19);
    jointAngles(:,1)  = data.([prefix 'pelvis_tX']); % PX
    jointAngles(:,2)  = data.([prefix 'pelvis_tY']); % PY
    jointAngles(:,3)  = data.([prefix 'pelvis_tZ']); % PZ
    jointAngles(:,4)  = data.([prefix 'pelvis_ry']); % PAX
    jointAngles(:,5)  = data.([prefix 'pelvis_rx']); % PFA
    jointAngles(:,6)  = data.([prefix 'left_thigh_rX']); % LHA
    jointAngles(:,7)  = data.([prefix 'left_thigh_rZ']); % LHF
    %jointAngles(:,8)  = 0; % LHE
    jointAngles(:,9)  = data.([prefix 'left_thigh_rZ']) - data.([prefix 'left_shank_rZ']); % LKF
    %jointAngles(:,10) = 0; % LAE
    %jointAngles(:,11) = 0; % LAP
    %jointAngles(:,12) = 0; % LAI
    jointAngles(:,13) = -data.([prefix 'right_thigh_rX']); % RHA
    jointAngles(:,14) = data.([prefix 'right_thigh_rZ']); % RHF
    %jointAngles(:,15) = 0; % RHE 
    jointAngles(:,16) = data.([prefix 'right_thigh_rZ']) - data.([prefix 'right_shank_rZ']); % RKF
    %jointAngles(:,17) = 0; % RAE
    %jointAngles(:,18) = 0; % RAP
    %jointAngles(:,19) = 0; % RAI    
end

function jointAngles = GetJointAnglesFromGUIMeasured(data, NSamples) %#ok<DEFNU> #ok<MSNU> 
    % data from therapist gui
    prefix = 'm_cont_jointAngles_';
    if ~isfield(data,[prefix 'PX'])
        jointAngles = [];
        return
    end;
    jointAngles = zeros(NSamples, 19);
    jointAngles(:,1)  =  data.([prefix 'PX']); % PX
    jointAngles(:,2)  =  data.([prefix 'PY']); % PY
    jointAngles(:,3)  =  data.([prefix 'PZ']); % PZ
    jointAngles(:,4)  =  data.([prefix 'PAX']); % PAX
    jointAngles(:,5)  =  data.([prefix 'POB']); % PFA ( Correct sign?)
    jointAngles(:,6)  =  data.([prefix 'LHA']); % LHA
    jointAngles(:,7)  =  data.( [prefix 'LHF']); % LHF
    %jointAngles(:,8)  = 0; % LHE
    jointAngles(:,9)  =  data.([prefix 'LKF']); % LKF
    %jointAngles(:,10) = 0; % LAE
    %jointAngles(:,11) = 0; % LAP
    %jointAngles(:,12) = 0; % LAI
    jointAngles(:,13) =  data.([prefix 'RHA']); % RHA
    jointAngles(:,14) =  data.([prefix 'RHF']); % RHF
    %jointAngles(:,15) = 0; % RHE 
    jointAngles(:,16) =  data.([prefix 'RKF']); % RKF
    %jointAngles(:,17) = 0; % RAE
    %jointAngles(:,18) = 0; % RAP
    %jointAngles(:,19) = 0; % RAI    
end

function jointAngles = GetJointAnglesFromGUIReference(data,NSamples) %#ok<DEFNU>
    % data from therapist gui
    prefix = 'r_cont_jointAngles_';
    if ~isfield(data,[prefix 'PX'])
        jointAngles = [];
        return
    end;
    jointAngles = zeros(NSamples, 19);
    jointAngles(:,1)  =  data.([prefix 'PX']); % PX
    jointAngles(:,2)  =  data.([prefix 'PY']); % PY
    jointAngles(:,3)  =  data.([prefix 'PZ']); % PZ
    jointAngles(:,4)  =  data.([prefix 'PAX']); % PAX
    jointAngles(:,5)  =  data.([prefix 'POB']); % PFA ( Correct sign?)
    jointAngles(:,6)  =  data.([prefix 'LHA']); % LHA
    jointAngles(:,7)  =  data.([prefix 'LHF']); % LHF
    %jointAngles(:,8)  = 0; % LHE
    jointAngles(:,9)  =  data.([prefix 'LKF']); % LKF
    %jointAngles(:,10) = 0; % LAE
    %jointAngles(:,11) = 0; % LAP
    %jointAngles(:,12) = 0; % LAI
    jointAngles(:,13) =  data.([prefix 'RHA']); % RHA
    jointAngles(:,14) =  data.([prefix 'RHF']); % RHF
    %jointAngles(:,15) = 0; % RHE 
    jointAngles(:,16) =  data.([prefix 'RKF']); % RKF
    %jointAngles(:,17) = 0; % RAE
    %jointAngles(:,18) = 0; % RAP
    %jointAngles(:,19) = 0; % RAI    
end

function jointAngles = GetTemplate(data,NSamples) %#ok<DEFNU>
    % Measured position on motorencoders (get measpos) with units in the fieldname
    prefix = 'm_cont_jointAngles_';
    if ~isfield(data,[prefix ''])
        jointAngles = [];
        return
    end;
    jointAngles = zeros(NSamples, 19);
    jointAngles(:,1)  = data.([prefix '']); % PX
    jointAngles(:,2)  = data.([prefix '']); % PY
    jointAngles(:,3)  = data.([prefix '']); % PZ
    jointAngles(:,4)  = data.([prefix '']); % PAX
    jointAngles(:,5)  = data.([prefix '']); % PFA
    jointAngles(:,6)  = data.([prefix '']); % LHA
    jointAngles(:,7)  = data.([prefix '']); % LHF
    jointAngles(:,8)  = data.([prefix '']); % LHE
    jointAngles(:,9)  = data.([prefix '']); % LKF
    jointAngles(:,10) = data.([prefix '']); % LAE
    jointAngles(:,11) = data.([prefix '']); % LAP
    jointAngles(:,12) = data.([prefix '']); % LAI
    jointAngles(:,13) = data.([prefix '']); % RHA
    jointAngles(:,14) = data.([prefix '']); % RHF
    jointAngles(:,15) = data.([prefix '']); % RHE 
    jointAngles(:,16) = data.([prefix '']); % RKF
    jointAngles(:,17) = data.([prefix '']); % RAE
    jointAngles(:,18) = data.([prefix '']); % RAP
    jointAngles(:,19) = data.([prefix '']); % RAI    
end
