======= GENERAL ===============================================================

This directory contains various files which can be convenient when post-processing
measured data from Lopes. 

Generally, there are three ways of getting data from Lopes:
1. Obtaining data from the therapist GUI.
   For each training, the therapist GUI generates a .signals, .steps and .params
   file. You can use the readXXX.m to read them into Matlab. The .signals file
   gives most information. You can show all data easily:
    	y=readSignalsFile
    	showSignalsData(y)
   With readSignalsFile, you can also give a directory or a filename as a
   parameter; in that case the directory will be shown in the file selection box, or
   the file will be opened directly.
   showSignalsData accepts more parameters as well, see 'help showSignalsData'.
  
   It may happen that readSignalsFile gives warnings about too long variable names.
   Just don't care. See also the help of showSignalsData; you can do cool stuff
   with it (select which signals to show and scale certain signals).
   
   Most signals from the GUI are called 'm_cont_XXX' or 'r_cont_XXX'. These stand 
   for 'measured values' (what the patient did) and 'reference values' (what the 
   patient should have done according to the computer). 'cont' refers to the 
   'continous' nature of the signals (as opposed to 'step, which is one 
   measurement per step).

2. Obtaining data from your own model on the HLC (via xPC Target).
   If you make your own model on the HLC to control Lopes, you can use xPC File scopes
   to log data. These will produce a .DAT file on the hard disk of the xPC. You can
   use the xpcexplr to transfer the file to your own computer. Then, you can use:
      y=readxpcfile(filename)
      figure; clf; 
      time = y.data(:,end); % Last column is time 
      plot(time, y.data(:,1:(end-1))); 
      legend(y.signalNames{1:end-1}); 
      xlabel(y.signalNames{end});
   
3. Obtaining data from the LowLevelController (LLC). For this, you need a
   separate Python application, called LLCDataLogger (download at 
   https://bitbucket.org/GijsvanOort/llcdatalogger) 
   It will generate a .signals file, which you can read identically to the
   .signals file from the therapist GUI (see 1).
   
   The fields in the file are up to you (you have a few hundreds to choose from).
   The most important are listed below.
	
======= DESCRIPTION OF ALL FILES ===============================================
AddCartesianPositions.m
  Given a data structure of a loaded .signals file (either from the GUI or from 
  the LLC), this function tries to augment the structure with cartesian 
  positions. It uses KinHuman_function.mexw64 and SourceForMexFuncion.zip (to 
  build your own KinHumanfunction.xxx) as dependencies.

akZoom2.m
  Used by showSingnalsData. Convenient (I think) tool for panning (left mouse 
  button) and zooming (right mouse button) in graphs. Be sure to _not_ select 
  the zoom or pan tool, and to click on an empty space in the graph (not on a 
  line). You can disable this functionality by removing/renaming akZoom2 or 
  removing the corresponding line in showSignalsData.

fastzoom.m
  Used by akZoom2. Dynamically reduces the number of points that needs to be 
  shown in the graph (omits points outside the current view and merges points 
  that would end up at the same pixel anyway). Hugely improves performance 
  during panning/zooming. You can disable this functionality by 
  removing/renaming fastzoom or removing the corresponding line in 
  showSignalsData.

KinHuman_function.mexw64
  See AddCartesianPositions.m
  
Readme.txt
  This file.

readParamsFile.m
readSignalsFile.m
readStepsFile.m
  Reads a .params, .signals or .steps file. Type help readXXXFile for more
  information.
  These files rely on ssdf.m for reading (part of) the file.

showSignalsData.m
  Given a structure from a loaded .signals file, this function shows all data
  in a graph. Becaue this is usually a lot of data, it begins with all signals
  hidden. You can show/hide each signal separately. This function uses akZoom2
  and fastzoom for more flexibility in zooming/panning.
  You can filter the signals to be shown in a powerful way. See 'help showSignalsData'
  for explanation.
  
setScaling.m
  Used by showSingnalsData. This function adds interactive rescaling (by factors of 10)
  of individual lines in the plot, which makes it more convenient to view signals
  of different orders of magnitude at the same time. This function can also be used
  stand-alone for any plot with multiple lines.
  
SourceForMexFuncion.zip
  See AddCartesianPositions.m
  
ssdf.m
  See readXXXFile.m
  
==== FIELDS IN THE .SIGNALS FILE (ONLY WHEN RECORDED WITH THE LLCDATALOGGER) ====
In older versions of the DataLogger, a prefix 'Lopes.' was present for each signal.
Note: many things are measured (more or less) redundantly.
Some recorded signals are the estimate based on one (type of) sensor only, other
signals are the best estimate we could get from doing sensor fusion.

Model.*
	The positions/angles of the virtual model (i.e., of the virtual masses).
	Directions are different from the ones that the HLC uses (because they're
	based on the LLC program): When you look along the (x,y,z)-axis into the
	positive direction, clockwise rotation is positive direction. Hence:
  
  ----------------------------------------------------------------------------
  Name           | Description                         | Positive orientation
  ----------------------------------------------------------------------------
  Pelvis TX      | forward/backward                    | Forward
  Pelvis TZ      | Left/right                          | Right
  Left Thigh RX  | Left Ab/addcuction                  | Abduction (!)
  Left Thigh RZ  | Left hip flexion/ext                | Flexion
  Left Knee RZ   | Left knee flexion/extension         | Extension
  Left Ankle RZ  | Left ankle plantar/dorsiflexion     | Dorsiflexion
  Right Thigh RX | Right Ab/addcuction                 | Adduction (!)
  Right Thigh RZ | Right hip flexion/ext               | Flexion
  Right Knee RZ  | Right knee flexion/extension        | Extension
  Right Ankle RZ | Right ankle plantar/dorsiflexion    | Dorsiflexion
  ----------------------------------------------------------------------------
  Left Shank RZ  | Absolute orientation of left shank  | Extension
  Right Shank RZ | Absolute orientation of right shank | Extension
  ----------------------------------------------------------------------------
  
Patient.Segments.Acceleration.AccMeasAcc*
  Joint accelerations as estimated from using solely the acceleration sensors
  close to the end effectors. 
  	
Patient.Segments.Position.PosMeasMotor*
	On the output shaft of each motor, there is an encoder. The positions of
	all joints can be calculated from these motor angles (assuming the system
	is perfectly stiff etc.). That is these values.
	
Patient.Segments.Force.*ForceFused
  The best estimate we have on the interaction force between human and machine,
  (and converted to joint torques). The knee torque is equal to the
	shank torque. The hip torque is equal to the thigh torque plus the shank torque.
	The torque is the torque that the human exerts on the system; a positive
	torque is when the human pushes the system into positive direction (and thus
	the system pushes the human into negative direction). Hence:
  
  --------------------------------------------------------------------
                 |                 Positive direction
  Name           | Human pushes robot into | Robot pushes human into
  --------------------------------------------------------------------
  Pelvis TX      | Forward                 | Backward
  Pelvis TZ      | Right                   | Left
  Left Thigh RX  | Abduction (!)           | Adduction (!)
  Left Thigh RZ  | Flexion                 | Extension
  Left Knee RZ   | Extension               | Flexion
  Left Ankle RZ  | Dorsiflexion            | Plantarflexion
  Right Thigh RX | Adduction (!)           | Abduction (!)
  Right Thigh RZ | Flexion                 | Extension
  Right Knee RZ  | Extension               | Flexion
  Right Ankle RZ | Dorsiflexion            | Plantarflexion
  --------------------------------------------------------------------
  Left Shank RZ  | Extension               | Flexion
  Right Shank RZ | Extension               | Flexion
  --------------------------------------------------------------------
	
Model.GlobalAccRefGain
  It is possible to do feedforward on acceleration level (see 'AccFeedForwardEnable'
  on page 7 of the documentation of the LopesDevelopmentLibrary). This value indicates
  whether it is on or off.
  
Model.* (acc Ref)
  The value of acceleration feed forward for each joint.
	
HapticRenderer.springspring.Stiffness*

  For each joint, the stiffness of the 'springspring' ('spring stiffnesses' in 
  the yellow library block of the LopesDevelopmentLibrary). Elements 6 and 10 
  refer to the ankle joints (which is not in the machine but still in the 
  software) and should be ignored. The other eight signals are the same as the 
  eight signals in the LopesDevelopmentLibrary.

HapticRenderer.springspring.Effect Pos *
  The equilibriumposition of each spring. For definition of positive, see the
  table of Output Force* below.
  
HapticRenderer.biasforcespring.Output Force*
	The forces that are exerted by the 'Bias Force' input of the Simulink
	model. The force is created by a segment spring with equilibrium position
	p_des=1000 (for each joint) and stiffness K= F_desired/1000 (for each joint).
	The resulting ouptut force is  
	   F_result = K * (p_des-p_actual) 
	            = F_desired/1000*(1000-p_actual)
		    = F_desired * (1- 0.001*p_actual)
	where p_actual may be a few tens (of a radian).
	When checking whether Lopes is able to track forces, you should consider
	these biasforcespring.OutputForce.* as the input, even though it may not
	be the force that you actually requested.
  
  Positive orientation is defined as resulting in a positive acceleration of the 
  virtual model and, as a consequnce, the robot itself and, as a consequence, 
  the human (assuming no other forces are present). Note: do not confuse these 
  with the measured forces Patient.Segments.Force.*ForceFused, which, depending 
  on which subsystem you ook at, may seem to have opposite sign! If you use the 
  robot in state Force, the robot tries to exert the force given here to the 
  human. Hence:

  ------------------------------------------------------------------------------
                  |                                  | Positive direction/
  Name            | Degree of freedom                | Robot pushes human into
  ------------------------------------------------------------------------------
  Output Force 1  | Forward/backward                 | Forward                 
  Output Force 2  | Left/right                       | Right                   
  Output Force 3  | Left Ab/addcuction               | Abduction (!)           
  Output Force 4  | Left hip flexion/ext             | Flexion                 
  Output Force 5  | Left knee flexion/extension      | Extension               
  Output Force 6  | Left ankle plantar/dorsiflexion  | Dorsiflexion            
  Output Force 7  | Right Ab/addcuction              | Adduction (!)           
  Output Force 8  | Right hip flexion/ext            | Flexion                 
  Output Force 9  | Right knee flexion/extension     | Extension               
  Output Force 10 | Right ankle plantar/dorsiflexion | Dorsiflexion            
  ------------------------------------------------------------------------------
    
Treadmill.Speed setpoint
Treadmill.Encoder.VelMeasInternalFiltered
	The setpoint and the measured velocity of the treadmill.

Treadmill.ForcePlate.*
  Measured CoP position and (filtered or not) FY and moments.
  Everything should be in N, but I guess the Fy is in kg.
  
State.current state number
  0: off; 1: init; 2: home; 3: stop; 4: position; 5:force  
  
Motor PVA Limiters.*.Cause of limit int
	Each motor has limits on the Position, Velocity and Acceleration (PVA).
	These are related, but not one-to-one-related, to joint limits.
	If any of the motors is limited, the corresponding value will be non-zero.
	1: Position limited. 2: Velocity limited. 3: Acceleration limited.
	
Patient.Human joints.*.Cause of limit.
	Also, each joint has its own PVA limits (for examplee, the kneee cannot
	hyperextend). Apart from the values 1,2 and 3, also 4 and 5 can occur, when
	a limit is touched due to another joint that's limited.

(Rod Space.RodAngles.*
  Approximately halfway between the motors and human legs, there is an extra set 
  of encoders which can be used to approxmiate the leg positions of the human 
  legs. The relation between 'end-effector positions' and these encoders is not 
  exactly determined yet. The Rod Space.RodAngles.* values are the estimations 
  of the human joint angles assuming an ideal mechanical structure. In the 
  future, we want to fuse these values with motor sensors, acceleration sensors 
  and force sensors in order to get the best estimate of the actual joint angles 
  of the human.)

sampletime
#Lopes.TimeSinceStartup
	On older versions of the DataLogger, these occur multiple times. The reason is
  that the amount of data was such that it had to be split into separate chunks
  that could be read simulataneously. The values of sampletime and
  #LopesTimeSinceStartup are used to sync the different chunks. In newer versions
  of the DataLogger, the multiple occurrences are removed automatically.
		
	 
	
	
	