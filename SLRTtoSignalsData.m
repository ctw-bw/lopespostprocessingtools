function Y=SLRTtoSignalsData(D)
% SLRTTOSIGNALSDATA  Convert SLRT (Filescope) data into Signals data
% 
% Takes a struct from the FileScope of Simulink Real-Time and returns a
% (reduced) signals data struct, which can be used by showSignalsData and
% splitIntoSteps.

Y.map = {'time',D.signalNames{1:end-1}};
Y.rawdata = D.data(:, [end,1:end-1]);

for i=1:length(Y.map)
    key = strrep2(Y.map{i}, {'.',' ','[',']','-','(',')','/','#',';'},{'_','_','_','_','dash','_','_','_','X','_'});
    Y.data.(key) = Y.rawdata(:,i);
end;

function y=strrep2(s, old, new)
% Similar to strrep, but this allows old and new to be equal-length cells
% Note: little error checking!
y=s;
for i=1:length(old)
    y = strrep(y, old{i}, new{i});
end;