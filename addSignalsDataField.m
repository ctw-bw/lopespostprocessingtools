function X=addSignalsDataField(X, fieldName, data, afterFieldName)
% function Y=addSignalsDataField(X, fieldName, data, [afterFieldName])
%
% Appends or inserts the given data with the given field name to the signalsData struct.
%
% fieldName may be either a string or a cell array with two or three strings, in
% which case it is 
%  { dataFieldName, mapFieldName } or
%  { dataFieldName, mapFieldName, unit } 
%
% If afterFieldName is not given, it appends the data (the new data becomes the last element).
% If afterFieldName is given, it inserts the new field directly
% after this field name. If the field name cannot be found, an error is
% given. The afterFieldName should match the name as given in X.map.
% At this moment, it is not possible to put a new field as the first field.
%
% Examples:
% interestingIndices = [D.data.Gamma==2 & D.data.Tau==3]
% D = addSignalsDataField(D, 'IsInteresting', interesingIndices)
%
% velocitiy = gradient(D.data.Position)*sampleTime;
% D = addSignalsDataField(D, {'J1_Velocity','J1 Velocity', 'rad/s'}, velocitiy, 'J1_Position')

% This function supports both the old and new SignalsStruct:
% * The old one, having a 'rawdata' matrix containing _all_ data and a
%   one-by-N 'map' (row vector), telling the human-readable name of each column,
% * The new one, lacking the 'rawdata' matrix, but having an extra field,
%   'datamap', where the fieldnames of the signals in the 'data' field are
%   stored in the right order.

assert(isrow(X.map),'X.map should be a row vector'); % The standard functions give a struct where X.map is a row vector; this function only works for those.

if isfield(X,'rawdata')
    isNewType = 0;
else
    isNewType = 1;
    assert(isrow(X.datamap),'X.datamap should be a row vector'); % The standard functions give a struct where X.datamap is a row vector; this function only works for those.
end


if ischar(fieldName)
    dataFieldName = fieldName;
    mapFieldName = fieldName;
    unit = '';
elseif iscell(fieldName)
    dataFieldName  = fieldName{1};
    mapFieldName = fieldName{2};
    if length(fieldName)==3
        unit = fieldName{3};
    else
        unit='';
    end
else
    error('Unknown data type for fieldName');
end

if ~exist('afterFieldName','var')
    % No afterFieldName given; simply append to the end; we do that by
    % supplying the index -1 to the insertAt function
    indexToInsertAt = -1;
else
    % Insert after afterFieldName
    indexToInsertAt = find(strcmp(X.map,afterFieldName));
    if isempty(indexToInsertAt)
        error('afterFieldName %s was not present in the Signals struct. Note that you should provide the name as in X.map (not fieldnames(X.data))', afterFieldName);
    end
    if numel(indexToInsertAt)>1
        warning('Provided afterFieldName %s occurs multiple times in the Signals Struct. Inserting the new data directly after the first occurrence.',afterFieldName);
        indexToInsertAt=indexToInsertAt(1);
        % now indexToInsertAt is always a scalar
    end
end

%% Do the work
X.data.(dataFieldName) = data(:);
if isNewType
    X.map = insertAt(X.map, {mapFieldName},indexToInsertAt);
    X.datamap = insertAt(X.datamap, {dataFieldName},indexToInsertAt);
else
    X.rawdata = [X.rawdata(:,1:ind), data(:), X.rawdata(:,ind+1:end)];
    X.map = insertAt(X.map, {mapFieldName},indexToInsertAt);
end

if isfield(X,'units')
    X.units = insertAt(X.units, {unit}, indexToInsertAt);
end
end

function arr=insertAt(arr, x,i)
    % Inserts element x in the array at place i (all elements >=i are shifted
    % up). Works for both row and column vectors (if arr is a scalar, it is
    % assumed to be a row vector.
    % To append to the end, use i=-1 (other negative values are not supported)\
    if i==-1
        i = length(arr)+1;
    end

    if i>=1 && i<=length(arr)+1
        if isrow(arr)
            arr = [ arr(1:(i-1)) , x, arr(i:length(arr))];
        else
            arr = [ arr(1:(i-1)) ; x; arr(i:length(arr))];
        end
    else
        error('Invalid value for index: %i (array has %i elements)',i, length(arr))
    end
end

    
