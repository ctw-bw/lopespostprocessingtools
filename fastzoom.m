function fastzoom(varargin)
% fastzoom()
% fastzoom(H) with H a figure handle
% fastzoom('update'), fastzoom(H,'update')
%          updates the figure (as if a pan or zoom action was
%          done). This assumes that the userdata is already filled in.
% Based on the jplot file (Jake Reimer, File Exchange:
% http://www.mathworks.nl/matlabcentral/fileexchange/42191-jplot)
%
%
% Applies automatic downsampling at low zoom factors and cropping at high
% zoom factors for faster zoom and pan. If no H (a handle to a figure) is
% given, the current figure is used.
%
% It works in subplots and can be combined with normals plots of any kind in the same axes.
%
% The amount of downsampling can be changed by adjusting the value of MAXPOINTS.
% Lines with less than MAXPOINTS are plotted normally.
% jplotting of multiple lines at once is allowed up to MAXHANDLES (otherwise everything is plotted normally). Adjust as necessary.
%
% Based on jplot (obtained from FileExchange)
%
% Note that jplot uses the 'tag' and 'userdata' properties of the plotted lines.
% v1.0 Jake Reimer 6/11/2013
% v1.1 Jake Reimer 6/12/2013 Takes initial xL as [min(x) max(x)] rather than initial xlims so that
%                            downsampling works even if you're zoomed into the axis when you jplot.  

MAXPOINTS=1E4;
MAXHANDLES=200;
%%
if nargin==0
    H = gcf;
    action = 'init';
elseif nargin == 1
    if ischar(varargin{1})
        H=gcf;
        action = varargin{1};
    else
        H=varargin{1};
        action = 'init';
    end;
elseif nargin==2
    H=varargin{1};
    action = varargin{2};
else
    error('Expected 0, 1 or 2 inputs, but got %d',nargin);
end;

if strcmp(action,'init')
    h=findobj(H,'type','line'); % Get all line elements in the current figure

    hZoom = zoom(H);
    hPan = pan(H);
    set(hZoom, 'ActionPostCallback', @zoomCallback);
    set(hPan , 'ActionPostCallback', @panCallback);


    if length(h) < MAXHANDLES
        for i=1:length(h)
            x=get(h(i),'xdata');
            len = length(x);
            if len > MAXPOINTS
                y=get(h(i),'ydata');

                raw.x=x;
                raw.y=y;
                raw.xL=[min(x) max(x)];
                raw.maxPoints=MAXPOINTS;

                set(h(i),'userdata',raw,'tag','dec');
                dec = ceil(len/MAXPOINTS);

                [xx,yy] = smartDecimatePoints(raw.x,raw.y,dec);
                set(h(i),'xdata',xx,'ydata',yy);
            end
        end
    end
elseif strcmp(action,'update')
    ev.Axes = findobj(H,'type','Axes');
    panCallback(0, ev);
else
    error ('unknown action %s',action)
end;
end

function zoomCallback(obj,ev)
    if strcmp(get(obj,'selectiontype'),'normal')
        xL=get(ev.Axes,'xlim');
    else
        xL=[-inf inf];
    end;
    doZoom(ev,xL);
end

function panCallback(~,ev)
    xL=get(ev.Axes,'xlim');
    doZoom(ev,xL);
end

function doZoom(ev,xL)        
h=findobj(ev.Axes,'tag','dec');
isVisible=get(h,'visible'); % only process the visible data
for i=1:length(h)
    if strcmp(isVisible(i),'on')
        raw=get(h(i),'userdata');
        len = length(raw.x);
        ind = [find(raw.x>=xL(1)-diff(xL),1,'first') find(raw.x<=xL(2)+diff(xL),1,'last')];
        if length(ind)==2
            % normal case
            dec = ceil(len/raw.maxPoints);
            dec = floor(dec * (diff(xL)/diff(raw.xL)))+1;

            [xx,yy] = smartDecimatePoints(raw.x(ind(1):ind(2)),raw.y(ind(1):ind(2)),dec);
            set(h(i),'xdata',xx,'ydata',yy);    
        else
            % No points inside the view
            % It would be better to include some points so that, on zooming
            % out, we get some view. But at least, we don't get an error
            % now.
            dn = get(h(i),'displayName');
            if strcmp(dn,'')
                fprintf(1,'Warning: no data points in x-range for line #%i\n',i);
            else
                fprintf(1,'Warning: no data points in x-range for line %s\n',dn);
            end;
            set(h(i),'xdata',[],'ydata',[]);
        end;
    end;
end
drawnow
end


function [x_dec,y_dec] = smartDecimatePoints(x,y,decc)
% Intelligent decimation. It chops x and y in pieces of 'decimation' long,
% and returns the minimum point and maximum point of each piece. 
% Normal decimation yields output having length length(x)/decimation, but
% because this function returns both the maximum and minimum, it has length
% 2*length(x)/decimation.
%
% Assumes that x and y are row vectors.
if decc<=2 % Then decimation makes no sense
    x_dec = x;
    y_dec = y;
else
    % We remove some points from the end of x (that's faster than including
    % them, and only removes a very small part of the data).
    nDecimated = fix(length(x)/decc);
    nPointsToInclude = nDecimated*decc;
    
    offset=0:decc:(nPointsToInclude-decc); % Offsets for indices
    y_reshape= reshape(y(1:nPointsToInclude),decc, nDecimated); % Now each column contains the data for one final point
    [~,i_min] = min(y_reshape);
    [~,i_max] = max(y_reshape);
    i_min=i_min+offset;
    i_max=i_max+offset;
    ii=sort([i_min, i_max]);
    x_dec = x(ii); y_dec = y(ii);
end;
end
