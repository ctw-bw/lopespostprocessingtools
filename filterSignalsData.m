function Y=filterSignalsData(X, indices)
% function Y=filterSignalsData(X, indices)
%
% Returns a struct Y containing all elements of X, but only the given
% indices. Indices can be a range or a logical. Example:
%
% interestingIndices = D.data.DataIsInteresting==1;
% DD = filterSignalsData(D, interesingIndices)

Y=X; % Full copy; we'll truncate after this

if isfield(Y,'rawdata')
    % For the 'new' signalsData struct, this field is not available.
    Y.rawdata = Y.rawdata(indices,:);
end

f = fieldnames(Y.data);
for i=1:length(f)
    ff = f{i};
    temp = Y.data.(ff);
    Y.data.(ff) = temp(indices);
end
    
