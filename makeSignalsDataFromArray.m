function Y=makeSignalsDataFromArray(y, signalNames, appendTo, oldnew)
% function Y=makeSignalsDataFromArray(y, [signalNames], [appendTo], [oldnew])
%
% Given an array with data y and a cell array of signalNames, this function
% returns a signalsData-compatble struct. 
% If signalNames is omitted or if it is [], default signals 'signal01', 
% 'signal02', ... are used.
% 
% If a signalsData-compatible struct is also given as one of the arguments,
% it appends the given data to this. Note that there is not so much
% error-checking involved here (e.g., if the data in y/signalNames does not
% have all data fields present in appendTo, it just adds the present
% fields, leaving the other datafields in appendTo untouched). Also note
% that if you want to use this option, you must provide a signalNames
% variable (may be []) as well. As appendTo, you can also give [], which
% behaves the same as not supplying any appendTo variable
%
% This function was made specifically for using with ToFile arrays (e.g.,
% when used in TwinCAT). Note that for those files, the rows of y are the
% data; not the columns. The first row is always time. Hence, if
% signalNames is given, numel(signalNames) should be equal to size(y,1)-1.
% (don't include 'time' in the signalNames array).
% 
% In TwinCAT, you get separate smaller data files, which can be
% concatenated in two ways. The first (old) way:
%   names = {'position','velocity','torque'};
%   load('data_part4.mat'); % Give variable y_4
%   load('data_part5.mat'); % Give variable y_5
%   Y = makeSignalsDataFromArray( [ y_4, y_5], names); 
% The scond (new) way, which uses much less memory and is faster:
%   names = {'position','velocity','torque'};
%   load('data_part4.mat'); % Give variable y_4
%   Y = makeSignalsDataFromArray( y_4, names); 
%   clear y_4;
%   load('data_part5.mat'); % Give variable y_5
%   Y = makeSignalsDataFromArray( y_5, names, Y); 
%   clear y_5;
%
% Optionally, as a third argument you can give either the string 'old' or
% 'new'. This refers to the type of SignalsStruct you want to get:
% * The old one, having a 'rawdata' matrix containing _all_ data and a
%   one-by-N 'map' (row vector), telling the human-readable name of each column,
% * The new one, lacking the 'rawdata' matrix, but having an extra field,
%   'datamap', where the fieldnames of the signals in the 'data' field are
%   stored in the right order.
% Default: 'new'

if ~exist('signalNames','var') || isempty(signalNames)
    signalNames = sprintfc('signal%02d',1:(size(y,1)-1));
    disp('Using default signal names (''signal01'', ''signal02'', ...)')
else
    assert (numel(signalNames) == size(y,1)-1, sprintf('Wrong number of signal names  (%d signal names, but data expected %d). Don''t include ''time'' in your list; it is added automatically.',numel(signalNames),size(y,1)-1 ));
end

if ~exist('appendTo','var')
    appendTo = [];
end

if ~exist('oldnew','var')
    oldnew = 'new';
end

if strcmp(oldnew,'old')
    % Old data structure
    if ~isempty(appendTo)
        error('using the appendTo argument is not supported for the old signalsData type');
    end
    
    Y.rawdata = y';
    Y.map = [ {'time'}, signalNames(:)'];

    for i=1:numel(Y.map)
        nme = Y.map{i};
        Y.data.(nme) = Y.rawdata(:,i);
    end
else
    % New data structure
    if isempty(appendTo)
        % Make a new structure and fill it
        map = [ {'time'}, signalNames(:)'];
        Y.map = map;
        Y.datamap = map;

        for i=1:numel(map)
            nme = map{i};
            Y.data.(nme) = y(i,:)';
        end
    else
        % Append to existing structure. We don't need to do anything with
        % the map.
        Y = appendTo;
        map = [ {'time'}, signalNames(:)'];
        for i=1:numel(map)
            nme = map{i};
            Y.data.(nme) =  [Y.data.(nme); y(i,:)'];
        end
    end
end