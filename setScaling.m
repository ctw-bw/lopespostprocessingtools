function setScaling(ax, scaling)
% setScaling(ax, [scaling])
%
% Sets the scaling of all lines in the given axis to some value. If the variable 
% scaling is missing, an interactive window is shown in which you can edit
% it. The variable scaling may have elements NaN, which means 'don't change
% the scaling of this line'.
% Scaling can also be set to 'auto', which does automatic scaling (so that
% all lines fall between -1..1).
%
% This function uses the gca's userdata (adds a struct in it with a field
% 'scaling' and was designed to be part of showSignalsData (i.e., not a
% separate tool; although it would work for any graph).

if nargin==1
    InteractiveWindow(ax);
elseif nargin == 2
    if strcmp(scaling,'auto')==1
        autoScaling(ax)
    else
        doSetScaling(ax,scaling);
    end;
else
    error([mfilename,': Wrong number of arguments (expected 1 or 2)']);
end;
end

    
function doSetScaling(ax,scaling)
    % This function does the actual work
    
    % Check if scaling has the right number of elements
    c = flipud(get(ax,'children')); % The order of children is reverse with the order appearing in the plot list
    % There can be 'ConstantLine's in te graph (marker lines), they should
    % not appear in the list, so just get the 'Line' elements
    c = c(strcmp(get(c,'type'),'line')); % Filter to get only the 'Line' elements

    assert(length(c)==length(scaling),sprintf('Expected %d scaling values, but received %d',length(c),length(scaling)));

    % Do the real work. In order to be compliant with fastzoom, we don't only
    % affect the ydata, but also the userdata|y (if it exists)
    % Also, we need to look at previous values of scaling (if they exist)
    u = get(ax,'userdata');
    if ~isfield(u,'scaling')
        % Then there was no scaling element, so make one up
        oldScaling = ones(size(scaling));
    else
        oldScaling = u.scaling;
        assert(length(oldScaling)==length(c),'Previous scaling data doesn''t have right size (did you add lines to the plot?).');
    end;
    
    % If there are any NaN's in the new scaling, we need to use the old
    % scaling values instead
    scaling(isnan(scaling))=oldScaling(isnan(scaling));

    relativeScaling = scaling./oldScaling; % By how much should we scale the current values?

    for i=1:length(c)
        cc = c(i);
        yd = get(cc,'ydata');
        set(cc,'ydata',yd*relativeScaling(i));

        % Opionally, do this for the userdata to (to be compliant with
        % fastZoom)
        uu = get(cc,'userdata');
        if all(isfield(uu,{'x','y','xL','maxPoints'}))
            % Then this line is managed by fastZoom; modify the y value (which
            % is the original data).
            uu.y = uu.y * relativeScaling(i);
            set(cc,'userdata',uu);
        end;

        % If needed, change name
        name = get(cc,'DisplayName');
        % Check if there was a scaling; if so, remove it
        y = regexp(name,'\(x [0-9\.e-]*\)');
        if length(y)==1
            name = name(1:y-2);
        end;
        % Add new scaling if needed
        if scaling(i)~=1
            name = [name ' (x ' num2str(scaling(i)),')'];
        end;
        set(cc,'DisplayName',name);
    end;

    set(ax,'userdata',struct('scaling',scaling));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function autoScaling(ax)
    % Automatically sets the scaling such that the abs max of each signal is <=1 (and >0.1)
    % if possible
   
    % Check if scaling has the right number of elements
    c = flipud(get(ax,'children')); % The order of children is reverse with the order appearing in the plot list
    % There can be 'ConstantLine's in te graph (marker lines), they should
    % not appear in the list, so just get the 'Line' elements
    c = c(strcmp(get(c,'type'),'line')); % Filter to get only the 'Line' elements

    
    
    % Do the real work. In order to be compliant with fastzoom, we don't only
    % affect the ydata, but also the userdata|y (if it exists)
    % Also, we need to look at previous values of scaling (if they exist)
    u = get(ax,'userdata');
    if ~isfield(u,'scaling')
        % Then there was no scaling element, so make one up
        oldScaling = ones(size(scaling));
    else
        oldScaling = u.scaling;
        assert(length(oldScaling)==length(c),'Previous scaling data doesn''t have right size (did you add lines to the plot?).');
    end;
    
    % Find, for each line, the abs max and calculate the relative scaling
    % and new scaling from it.
    

    relativeScaling = nan(1,length(c));
    for i=1:length(c)
        cc = c(i);
        yd = get(cc,'ydata');
        mx = max(abs(yd));
        if mx==0
            % Special case; don't affect scaling
            relativeScaling(i)=1;
        else
            relativeScaling(i) = 10^-ceil(log10(mx));
        end;
    end;
    newScaling = oldScaling .* relativeScaling;
    
    % Limit to what we can show with the pulldown box
    newScaling(newScaling<1e-6) = 1e-6;
    newScaling(newScaling>1e6) = 1e6;

    doSetScaling(ax, newScaling);

    %Check if there is an interactive window; if so, refresh it
    f = findobj('name','Set scaling');
    if ~isempty(f)
        setScaling(ax); % Rebuilds the window
    end;
%     set(ax,'userdata',struct('scaling',scaling));
% end    
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function InteractiveWindow(ax)
    % Parameters
    height = 23;
    spacing=2;
    pulldownwidth=100;
    verticalTextOffset = -9;
    hspacing=5;
    fontsize = 10;
    
    % Subfunctions
    function resizeFigure(f,~)
        
        % Get the number of lines
        u=get(f,'userdata');
        n = u.n;
        
        % Panel height is dependent on n
        panelHeight = (n+1)*(height+spacing)+spacing;
        
        % Resize panel and slider
        sz = get(f,'position');
        c =get(f,'children');
        set(u.panel,'position',[0,0,sz(3)-30,panelHeight]); % UIpanel
        set(u.slider,'position',[sz(3)-20,0,20,sz(4)]); % Slider
    
        % Set slider properties
        if panelHeight < sz(4)
            panelHeight = sz(4);
            set(u.slider,'visible','off');
            set(u.panel,'position',[0,0,sz(3),panelHeight]); % UIpanel
            
        else
            set(u.slider,'position',[sz(3)-20,0,20,sz(4)]); % Slider
            set(u.slider,'visible','on');
            v = get(u.slider,'value');
            newmax = panelHeight-sz(4);
            nLinesVisible = floor(sz(4)/(height+spacing));
            % How many lines are missing? (that's the number of steps
            % needed)
            nLinesMissing = newmax/(height+spacing);
            if v>newmax, 
                v=newmax;
                set(u.slider,'value',v);
            end;
            
            set(u.slider,'min',0,'max',newmax,'sliderstep',[1,nLinesVisible]/nLinesMissing);
            set(u.panel,'position',[0,-v,sz(3)-30,panelHeight]); % UIpanel
            
        end;
        
        % Resize elements in the panel
        szp = get(u.panel,'position'); % panel size (and position)
        for ii=1:length(u.labels)
            y = panelHeight - (ii+1)*(height+spacing)+5;
            set(u.pulldowns(ii),'position',[1,y,pulldownwidth,height-5]);
            set(u.labels(ii),'position',[pulldownwidth+hspacing, y+verticalTextOffset, szp(3)-pulldownwidth-hspacing-3,height]);
        end;
        set(u.autoButton,'position',[0,panelHeight-height-3,100,height]);
    end

    function setScalingSingle(ax, lineIndex, hnd)
        % Adjusts the scaling for one singe line (keeps others the same)
        scales = get(hnd,'userdata');
        newScaling = scales(get(hnd,'value'));
        
        c = get(ax,'children');
        % There can be 'ConstantLine's in te graph (marker lines), they should
        % not appear in the list, so just get the 'Line' elements
        c = c(strcmp(get(c,'type'),'line')); % Filter to get only the 'Line' elements

        sc = nan(1,numel(c));
        sc(lineIndex) = newScaling;
        doSetScaling(ax,sc);
    end

    function scrollFcn(~, eventdata, ~)
        u=get(f,'userdata');

        % VerticalScrollCount is positive or negative depending on 
        % the direction of the rotation. It can also change in value 
        % depending on the rotation speed.
        Nscroll = eventdata.VerticalScrollCount;
        v=u.slider.Value;
       	u.slider.Value = max([0,min([u.slider.Max, v - Nscroll*(height+spacing)])]); 
        resizeFigure(f,[]);
    end

    c = flipud(get(ax,'children')); % The order of children is reverse
    % There can be 'ConstantLine's in te graph (marker lines), they should
    % not appear in the list, so just get the 'Line' elements
    c = c(strcmp(get(c,'type'),'line')); % Filter to get only the 'Line' elements
    
    u = get(ax,'userdata');
    if ~isfield(u,'scaling')
        % Then there was no scaling element, so make one up
        oldScaling = ones(size(scaling));
    else
        oldScaling = u.scaling;
        assert(length(oldScaling)==length(c),'Previous scaling data doesn''t have right size (did you add lines to the plot?).');
    end;
    
    % Is there an old scaling window?
    f = findobj('name','Set scaling');
    if isempty(f)
        f = figure();
    else
        f=f(end); % Take last figure; that's probably the most recent one
        figure(f);
        clf;
    end;

    set(f,'name','Set scaling','NumberTitle','off','Menubar','none','units','pixels');
    
    p = uipanel('units','pixels');
    set(f, 'WindowScrollWheelFcn', @scrollFcn);
    sl = uicontrol('style','slider','max',9999999,'value',9999999); % Just large so that we always start at the top
    autoButton = uicontrol('style','pushbutton','parent',p,'string','Auto');
    for i=1:length(c)
        name = get(c(i),'DisplayName');
        % Check if there was a scaling; if so, remove it
        y = regexp(name,'\(x [0-9\.]*\)');
        if length(y)==1, name = name(1:y-2); end;
        currentScaling = oldScaling(i);
        scalingList = logspace(-9,9,19);
        if ~any(scalingList==currentScaling)
            % Then add the current scaling to the list
            scalingList = sort([scalingList currentScaling]);
        end;
        temp= arrayfun(@(x)['|',num2str(x)],scalingList,'uniformoutput',0);
        temp{1}=temp{1}(2:end); 
        scalingListString= strcat(temp{:});
        
        % Add items
        pulldowns(i) = uicontrol('style','popup','string',scalingListString,'parent',p,'fontsize',fontsize); %#ok<AGROW>
        labels(i) = uicontrol('style','text','string',name,'parent',p,'horizontalAlignment','left','fontsize',fontsize); %#ok<AGROW>
        set(pulldowns(i),'value',find(currentScaling==scalingList));
        set(pulldowns(i),'userdata',scalingList); % Numerical list, easier to lookup data
        set(pulldowns(i),'callback', @(h,~)setScalingSingle(ax, i, h));        
    end;
    
    set(f,'userdata',struct('n',length(labels),'labels',labels,'pulldowns',pulldowns,'panel',p,'slider',sl,'autoButton',autoButton));
    
    resizeFigure(f); % This makes all the controls go into the right place
    set(f,'resizefcn',@resizeFigure);
    set(sl,'callback',@(~,~)resizeFigure(f,[])); % Also takes care of sliding.
    set(autoButton,'callback',@(~,~)setScaling(ax,'auto'));
end    