function showSLRTData(d,varargin)
% SHOWSLRTDATA  Shows an interactive plot window for viewing SLRT (file scope) structures.
%
% Usage:
%   showSLRTData(d, [filterRegex, [scaling, [filterRegex, [scaling, []]]]])
%
% Given the datastructure d from a File Scope (by readxpcfile), it shows all data in a plot.
%
% After d, any number of tuples (filterRegex, scaling) can be given. The
% last tuple can omit the scaling value (defaults to 1).
%
% if filterRegex, a regular expression, is given, it will only show the
% fields whose name match the regular expression case-insensitively.
%
% Example:
% showSLRTData(d,'jointAngles.*(LHA|RHA|PZ)',1,'Forces.*(LHA|RHA|PZ)',0.01,'phase')

% This function works by using showSignalsData. First we set up a new
% struct with the correct names so that it is compatible with
% showSingalsData, then we call the function itself.
% We need to put time (the last column) to the front

d2 = SLRTtoSignalsData(d);
showSignalsData(d2, varargin{:});
