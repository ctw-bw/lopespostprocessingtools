function showSignalsData(d,varargin)
% SHOWSIGNALSDATA  Shows an interactive plot window for viewing signalsData structures.
%
% Usage:
%   showSignalsData(d, [filterRegex, [scaling, [filterRegex, [scaling, []]]]])
%
% Given the datastructure from a SignalsFile d, it shows all data in a plot.
%
% After d, any number of tuples (filterRegex, scaling) can be given.
% The last tuple can omit the scaling value (defaults to 1).
%
% if filterRegex, a regular expression, is given, it will only show the
% fields whose name match the regular expression case-insensitively.
%
% Example:
% showSignalsData(d,'jointAngles.*(LHA|RHA|PZ)',1,'Forces.*(LHA|RHA|PZ)',0.01,'phase')


% Parse include sets.
n=length(varargin);
if n==0 % Set to default values
    varargin={'.*',1};
    nSets=1;
elseif rem(n,2)==1
    % Complete such that each filterRegex also has a scaling
    varargin{end+1} = 1;
    n=n+1;
    nSets=n/2;
else
    nSets=n/2;
end
%nSets is now the number of (filterRegex,scaling) tuples.

% We support two types of SignalsData struct:
% * The old one, having a 'rawdata' matrix containing _all_ data and a
%   one-by-N 'map' (row vector), telling the human-readable name of each column,
% * The new one, lacking the 'rawdata' matrix, but having an extra field,
%   'datamap', where the fieldnames of the signals in the 'data' field are
%   stored in the right order.
% Below, we create one function that, depending on the type, has a different
% way of getting the data. Then, later we can use this one function,
% regardless of the type.
% In both cases, getData(i) is a function which returns the data of the
% i'th signal (as seen in d.map).
if isfield(d,'rawdata')
    getData= @(i) d.rawdata(:,i);
    getSignalName=@(i) d.map{i};
else
    getData= @(i) d.data.(d.datamap{i});
    getSignalName=@(i) d.map{i};
end

tme = d.data.time;

if isfield(d,'units')
    u = d.units;
    useUnits = 1;
else
    useUnits = 0;
end
figure(199);
clf; 
i=0;
allscaling = [];
for nn= 1:nSets
    filterRegex = varargin{nn*2-1};
    scaling = varargin{nn*2};
    for n = 1:length(d.map)
        name = getSignalName(n);
        if useUnits
            dispUnit = [' [' u{n} ']'];
        else
            dispUnit = '';
        end
        % Filter using regexp
        y = regexpi(name,filterRegex);
        if ~isempty(y)
            i=i+1;
            if scaling==1
                dispName = [name dispUnit];
                allscaling(end+1) = 1; %#ok<*AGROW>
            else
                dispName = [name dispUnit ' (x ' num2str(scaling),')'];
                allscaling(end+1) = scaling;
            end
            P(i) = plot(tme, getData(n)*scaling,'visible','off','DisplayName',dispName);
            
            hold on;
        end
    end
end

% Add indices entry (so that you can find out which index is associated
% with a certain time)
dispName = '-indices-';
allscaling(end+1) = 1;
P(i+1) = plot(tme, 1:length(tme),'visible','off','DisplayName',dispName);

hold off;
set(gca,'userdata',struct('scaling',allscaling));
xlabel('time (s)');
colors = brighten(hsv(i),-0.5);
colors = colors(randperm(i),:);
for n=1:i
    set(P(n),'color',colors(n,:));
end

plotbrowser('on');
grid on
set(gcf,'numbertitle','off','name','SignalsData')

if exist('akZoom2','file')
    akZoom2
end

% Hide all fields by default so we can enable only those we're interested in
if i<10
    set(get(gca,'children'),'visible','on');
end

% Add button for changing the scaling
pushbuttons(1) = uicontrol('style','pushbutton','callback',@(~,~)setScaling(gca),'string','Set scaling');
pushbuttons(2) = uicontrol('style','pushbutton','callback',@(~,~)setScaling(gca,'auto'),'string','Auto scaling');
pushbuttons(3) = uicontrol('style','pushbutton','callback',@(~,~)FitXAxis(gca, d, P),'string','Fit X axis');
pushbuttons(4) = uicontrol('style','pushbutton','callback',@(~,~)ylim('auto'),'string','Fit Y axis');
pushbuttons(5) = uicontrol('style','pushbutton','callback',@(~,~)SetDateTimeOnXAxis(d,P, gcf, gca),'string','DateTime X-axis');
pushbuttons(6) = uicontrol('style','pushbutton','callback',@(hnd,~)redrawGraph(gca),'string','Redraw');
pushbuttons(7) = uicontrol('style','pushbutton','callback',@(hnd,~)toggleHitTest(P,hnd,'toggle'),'string','Toggle HitTest');
pushbuttons(7).TooltipString = 'For selecting lines and using the ''Data Cursor'' tool, HitTest should be on. For panning/zooming with mouse drag, it is most convenient if HitTest is off.';
% Update the text on the ToggleHitTest button
toggleHitTest(P, pushbuttons(7), 'query');

% Set buttons at correct spot
% Parameters
height = 20; xspacing = 5;
widths = [60,65,50,50,80,50,115]; 
ox = 20; oy=20; % Lower left position of first button
% Do the work
oxx = ox + [0 cumsum(widths(1:end-1))] + (0:(length(widths)-1))*xspacing;
for i=1:length(pushbuttons)
  set(pushbuttons(i),'position', [oxx(i), oy, widths(i), height]);
end

end


function SetDateTimeOnXAxis(dd,PP, F ,A) %#ok<INUSL>
 datetimeoffset = datenum([dd.date,'_',dd.time], 'yymmdd_HHMMSS'); % unit is days
 newxdata = datetimeoffset + dd.data.time/(60*60*24);
 
 % Calculate new x limits
 xl =xlim(A);
 newxl = datetimeoffset + xl/(60*60*24);
 
 % Usually, fastzoom will be active; then we need to adjust the
 % userdata and let fastzoom update the data itself. Otherwise, we just
 % update the xdata of the line
 if all(isfield(PP(1).UserData,{'x','y','xL','maxPoints'}))
     % fastZoom is active
     for i=1:length(PP)
        PP(i).UserData.xdata = newxdata;
     end
     xlim(A, newxl);
%      fastzoom(F, 'update');
 else
     % fastZoom is not active
     for i=1:length(PP)
        PP(i).XData = newxdata;
     end  
      xlim(A, newxl);
 end
 datetick(A, 'x','HH:MM:SS', 'keeplimits'); 
end

function redrawGraph(G)
    fastzoom(G.Parent, 'update');
end

function toggleHitTest(P, Button, command)
% Turns on or off hittest for all lines. Valid options for command:
% 'on', 'off' - turns all on or off
% 'toggle' - Looks at the first hittest value and makes all lines the opposite of this
% 'query' - Investigates the hittest state of all lines 
% This function will always update the text on the button Button to reflect the
% current state of hitTest.
switch command
    case 'toggle'
        if strcmp(P(1).HitTest,'on')
            toggleHitTest(P,Button,'off');
        else
            toggleHitTest(P,Button,'on');
        end
    case 'on'
        set(P,'HitTest','on');
    case 'off'
        set(P,'HitTest','off');
    case 'query'
        % don't do anything to the hittest state
    otherwise
        error('Unknown command in showSignalsData|toggleHitTest: %s',command);
end

% Update the text on the button
if all(strcmp(get(P,'hittest'),'off'))
    Button.String='Toggle HitTest (now off)';
elseif all(strcmp(get(P,'hittest'),'on'))
    Button.String='Toggle HitTest (now on)';
else
    Button.String='Toggle HitTest (now mixed)';
end

end % of fucntion toggleHitTest

function FitXAxis(ax,dd, PP)
    xlim(ax, [dd.data.time(1),dd.data.time(end)]);
    
    % Check if fastzoom is active
    if all(isfield(PP(1).UserData,{'x','y','xL','maxPoints'}))
        fastzoom('update');
    end
end % of function FitXAxis
