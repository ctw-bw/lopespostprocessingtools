function Y = splitIntoSteps(D, varargin)
% Given 'signalsdata' struct D, this function tries to separate the data
% into separate steps. Various arguments can be given to influence the
% behaviour. The data is always resampled/interpolated.
% This function works for SLRT data as well
%
% The output struct Y is as follows:
%   Y.data.(fieldname):
%      A matrix of which each column represents one step (i.e., the number
%      of colunms equals the number of steps recorded).
%   Y.rawdata:
%      an array of structs, each of which has the following fields:
%      Y.rawdata(i).name: The signal name
%      Y.rawdata(i).data: The data; format identical to Y.data.(fieldname)
%   Y.map:
%      A cell array containing all signal names. Thus,
%      Y.map{i} = Y.rawdata(i).name and
%      Y.data.(Y.map{i}) = Y.rawdata(i).data
%   Y.options:
%      A struct consisting of all options used in the conversion
%   Y.nSteps:
%      The number of steps extracted
%
% Options are given in the form of string, value pairs, e.g.:
%   Y = splitIntoSteps(D, 'maxStrideDuration',3, 'nPointsPerStride',101)
%
% The following options are supported (case insensitive):
%   'phaseSignal' (no default)
%      The name of the signal which indicates the phase of the gait, such
%      that the Matlab expression Y.data.(phaseSignal) would give
%      corresponding signal.
%   'timeSignal' (default: 'time')
%      The name of the signal which indicates the time in seconds, such
%      that the Matlab expression Y.data.(timeSignal) would give
%      corresponding signal.
%   'maxStrideDuration' (default: 3)
%      Any detected 'steps' longer than this many seconds are discarded
%      (probably the person wasn't walking anyway).
%   'nPointsPerStride' (default: 101)
%      The data is always interpolated/resampled so that each stride has
%      the same number of samples. This value is the number of samples used
%      for each stride.
%   'includeEndpoint' (default: 1)
%      If nonzero, the last point of each step is a point at 100%, which is
%      identical to the first point (0%) at the next step. You may not want
%      to include this point if you later want to concatenate steps
%      (because then you get double data).
%   'phaseDetectionMode' (default: '1234')
%      How the algorithm detects the start of a new stride. The following
%      values are supported:
%        '1234': Detects a new stride based on the phase in 1-2-3-4 (double
%          stance/right swing/double stance/left swing). The first sample at
%          which the phase is 1, is seen as 0% of the stride.
%        '100pct': Detects a new stride based on a phase signal in the
%          range of [0,100). For now, the first sample at which the phase is
%          close to zero (as opposed to close to 100) is seen as 0%. This is
%          somewhat inaccurate; it would be better to do inter-sample
%          interpolation to find the exact point where the phase is 0. This
%          mode is not implemented yet.
%        'event': Any non-zero value is seen as the start of a new stride.
%   'phaseEvolutionMode' (default: 'time')
%      If the start of step k and start of step k+1 are known, the data in
%      between can be interpolated. However, the phase (in 0-100%) may not
%      evolve linearly in time. This option sets the way the data is
%      interpolated. The following values are supported:
%        'time': The nPointsPerStride values are chosen such that they are
%           equidistant in time; e.g., the 'middle point' is always exactly
%           halfway between the starting time and ending time. This may not
%           correspond with 50% gait phase. (not implemented yet)
%        'phase': The nPointsPerStride values are chosen such that they are
%           equidistant in phase; e.g., the 'middle point' is always exactly
%           at 50%. This may not correspond to halfway between start and
%           end time.

%% Parse input arguments and build a struct with all options
if mod(nargin,2)~=1
    % We need an odd number of input arguments
    error('splitIntoSteps:wrongNumberOfInputArguments','splitIntoSteps needs an odd number of input arguments. See help splitIntoSteps.');
end

options.phaseSignal = ''; % No default
options.timeSignal = 'time';
options.maxStrideDuration = 3;
options.minStrideDuration = 0.1;
options.nPointsPerStride = 101;
options.includeEndpoint = 1;
options.phaseDetectionMode = '1234';
options.phaseEvolutionMode = 'time';

phaseSignalSet = false;
for i=1:2:(nargin-1)
    key = varargin{i};
    value = varargin{i+1};

    switch lower(key)
        case 'phasesignal'
            options.phaseSignal = value;
            phaseSignalSet = true;
        case 'timesignal'
            options.timeSignal = value;
        case 'maxstrideduration'
            options.maxStrideDuration = value;
        case 'minstrideduration'
            options.minStrideDuration = value;
        case 'npointsperstride'
            options.nPointsPerStride = value;
        case 'includeendpoint'
            options.includeEndpoint = value;
        case 'phasedetectionmode'
            switch lower(value)
                case '100pct'
                    options.phaseDetectionMode = '100pct';
                case '1234'
                    options.phaseDetectionMode = '1234';
                case 'event'
                    options.phaseDetectoinMode = 'event';
                otherwise
                    error('splitIntoSteps:valueError', 'unknown value for phaseDetectionMode: %s',value);
            end
        case 'phaseevolutionmode'
            switch lower(value)
                case 'time'
                    options.phaseEvolutionMode = 'time';
                case 'phase'
                    options.phaseEvolutionMode = 'phase';
                otherwise
                    error('splitIntoSteps:valueError', 'unknown value for phaseEvolutionMode: %s',value);
            end
        otherwise
            error('splitIntoSteps:unknownOption','unknown option: %s', key);
    end % of switch
end

if ~phaseSignalSet
    error('splitIntoSteps:phaseSignalNotSet', '''phaseSignal'' option was not set. See help splitIntoSteps');
end
% Now the variable Options has all options in it (and the values should
% work as well, although no extensive value checking is done.

%% Check whether we have SLRT data; if so, convert to Signals Data
if isfield(D,'signalNames')
    D = SLRTtoSignalsData(D);
end
%% Detect stride starts
phaseValues = D.data.(options.phaseSignal);
timeValues = D.data.(options.timeSignal);

if strcmp(options.phaseDetectionMode,'1234')
    strideStartIndices = find(phaseValues(:)==1 & [1;phaseValues(1:end-1)]~=1); 
        % If the first element is 1, ignore it; the chance that this was indeed the start of a step is very small
    strideStartTimes = timeValues(strideStartIndices);
elseif strcmp(options.phaseDetectionMode,'event')
    strideStartIndices = find(phaseValues~=0); 
    strideStartTimes = timeValues(strideStartIndices);
elseif strcmp(options.phaseDetectionMode,'100pct')
    strideStartIndices = find( [0; diff(phaseValues)<-50]);
        % If the first element is 1, ignore it; the chance that this was indeed the start of a step is very small
    strideStartTimes = timeValues(strideStartIndices);
else
    error('splitIntoSteps:valueError', 'unknown value for phaseDetectionMode: %s',options.phaseDetectionMode);
end

%% Detect valid steps; generate a 2-row matrix in which each column consists of a start time and end time
strideStartEndTimes = [strideStartTimes(1:end-1)'; strideStartTimes(2:end)'];
strideStartEndData = [diff(strideStartEndTimes); strideStartEndTimes; strideStartIndices(1:end-1)'; strideStartIndices(2:end)']; % Add duration of each step and start/end indices
strideStartEndDataFiltered = strideStartEndData(:, strideStartEndData(1,:)>=options.minStrideDuration & strideStartEndData(1,:) <=options.maxStrideDuration); % Filter out all steps that are too small or too long
nStrides = size(strideStartEndDataFiltered,2);
% Each column of strideStartEndDataFiltered contains Start/End data for one
% stride: [Duration; StartTime; EndTime; StartIndex; EndIndex]


%% Create interpolated data interpolation
zros = zeros(options.nPointsPerStride, nStrides); % This is the size of each data array

if strcmp(options.phaseEvolutionMode,'time')
    x = timeValues;
    xq =  zros;
    for i=1:nStrides
        if options.includeEndpoint
            xq(:,i) = linspace(strideStartEndDataFiltered(2,i), strideStartEndDataFiltered(3,i), options.nPointsPerStride);
            normalizedTimeSeries = linspace(0,1,options.nPointsPerStride)';
        else
            temp = linspace(strideStartEndDataFiltered(2,i), strideStartEndDataFiltered(3,i), options.nPointsPerStride+1);
            xq(:,i) = temp(1:end-1);
            temp = linspace(0,1,options.nPointsPerStride+1);
            normalizedTimeSeries = temp(1:end-1)';
        end
    end
    % Now we can use x and xq for each field in interp1
    % Interpolate the data
    for i=1:length(D.map)
        Y.rawdata(i).name = D.map{i};
        Y.rawdata(i).data = interp1(x,D.rawdata(:,i),xq);
    end
elseif strcmp(options.phaseEvolutionMode,'phase')
%     x = phaseValues;
%     x  = 
    xq =  zros;
    for i=1:nStrides
        if options.includeEndpoint
            xq(:,i) = linspace(0,100,options.nPointsPerStride)'; % Phase is 0..100 by definition
            normalizedTimeSeries = linspace(0,1,options.nPointsPerStride)';
        else
            temp = linspace(0,100,options.nPointsPerStride+1);
            xq(:,i) = temp(1:end-1);
            temp = linspace(0,1,options.nPointsPerStride+1);
            normalizedTimeSeries = temp(1:end-1)';
        end
    end
    % Now we can use x and xq for each field in interp1
    for i=1:length(D.map)
        Y.rawdata(i).name = D.map{i};
        if strcmp(escape(D.map{i}),options.phaseSignal)
            % Special case, the data is simply xq (since this is the input
            % signal as well). We force it to be xq because with the normal
            % way (interp1), the last element is wrong.
            Y.rawdata(i).data = xq;
        else
            %Normal case
            Y.rawdata(i).data = zros;
            for n=1:nStrides
                % For each stride we only use the data of that specific stride
                iStart = strideStartEndDataFiltered(4,n); iEnd = strideStartEndDataFiltered(5,n);
                x = unwrap100(phaseValues(iStart:iEnd));
                Y.rawdata(i).data(:,n) = interp1(x,D.rawdata(iStart:iEnd,i),xq(:,n),'linear','extrap');
            end
        end
    end
else
    error('splitIntoSteps:valueError', 'unknown value for phaseEvolutionMode: %s',options.phaseEvolutionMode);
end


%% Generate Y.data and Y.map field
for i=1:length(Y.rawdata)
    key = escape(Y.rawdata(i).name);
    Y.data.(key) = Y.rawdata(i).data;
end

%% Add some special fields for indexing
% It's easiest to do this after generating the Y.data, because then we can
% access the timeSeries data by name.

% timeSinceStartOfStride
tt = Y.data.(options.timeSignal);
tt = tt - repmat(tt(1,:),options.nPointsPerStride,1); % Remove offset
Y.rawdata(end+1)= struct('name','timeSinceStartOfStride','data',tt);
Y.data.timeSinceStartOfStride = tt;

% normalizedTime
tt = repmat(normalizedTimeSeries,1,nStrides);
Y.rawdata(end+1)= struct('name','normalizedTime','data',tt);
Y.data.normalizedTime = tt;

Y.map={Y.rawdata.name}; % Update Y.map (more robust than adding the separate fields to it)

%% Final matter
Y.options = options; % Store the options for reference
Y.nSteps = size(Y.rawdata(1).data,2);

function y=escape(s)
% 'Escapes' strange characters that are not allowed in field names.
y=strrep2(s, {'.',' ','[',']','-','(',')','/','#',';'},{'_','_','_','_','dash','_','_','_','X','_'});

function y=strrep2(s, old, new)
% Similar to strrep, but this allows old and new to be equal-length cells
% Note: little error checking!
y=s;
for i=1:length(old)
    y = strrep(y, old{i}, new{i});
end

function y=unwrap100(x)
% Same as Matlab's unwrap, but this function unwraps between 0 and 100
% (instead of -pi..pi). We misuse Matlab's unwrap function for this.
cf = 2*pi/100; % Conversion factor
y = (unwrap( x * cf -pi)+pi)/cf; 